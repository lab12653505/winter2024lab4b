public class Horse {

	private double topspeed;
	private String breed;
	private String favoritefood;
	
	public Horse(double speed, String type, String food) {
			topspeed = speed;
			breed = type;
			favoritefood = food;
	}
	public static double runfaster(double speed) {
		speed = speed*2;
		System.out.println("He runs faster now!");
		return speed;
	}
	public static String changebreed(String type) {
		if (type == "mustang") {
			type = "Fjord horse";
		} else {
			type = "mustang";
		}
		System.out.println("His new type is: " + type);
		return type;
	}
	
	//getters and setters	
	public double getSpeed(){
		return this.topspeed;
	}

	public String getBreed(){
		return this.breed;
	}
	public void setFavFood(String food){
		this.favoritefood = food;
	}
	public String getFood(){
		return this.favoritefood;
	}
}